# ADMETOX_research

Compare Biofacquim ADME TOX perfil vs other libraries

Results and analysis of ADMET/Tox profile of NP Libraries, including Biofacquim

    Input_files -> Original databases

    Subsets_ADME -> contains  subsets with 100 elements

    ADMET_swiss -> results from SwissADME server

    ADMET_pkc -> results from pkc50 server

    Merge_results -> scripts to merge servers results

    Statistics -> Scripts to compute statistics

    Libraries-> Storage final results

	    Libraries_ADME -> Libraries results from SwissADME server (After merge)
	    
	    Libraries_ADME -> Libraries results from pkc50 server (After merge)
	
