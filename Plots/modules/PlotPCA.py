"""
provide help to visualice PCA results 
and explore chemical space
"""
from bokeh.io import show, output_file, export, export_png, export_svgs
from bokeh.models import (
    ColumnDataSource,
    LassoSelectTool,
    ZoomInTool,
    ZoomOutTool,
    SaveTool,
    HoverTool,
    PanTool,
    Legend,
)
from bokeh.plotting import figure
from bokeh.core.enums import LegendLocation

"""
column source, to allow bokeh plot
"""
import bokeh
from bokeh.models import ColumnDataSource


def column_source(result, Library):
    X = list()
    Y = list()
    N = list()
    DF = result[result["Library"] == Library]
    X = list(DF["PC 1"])
    Y = list(DF["PC 2"])
    N = list(DF["Name"])

    return ColumnDataSource(dict(x=X, y=Y, N=N))


class Plot:
    def __init__(self, result):
        self.result = result

    def plot_pca(self, parameter, a, b):
        result = self.result
        print(a, b)
        source1 = column_source(result, "FDA")
        source2 = column_source(result, "BIOFACQUIM")
        source3 = column_source(result, "AfroDB")
        source4 = column_source(result, "NuBBEDB")
        source5 = column_source(result, "TCM")
        hover = HoverTool(tooltips=[("PCA 1", "$x"), ("PCA 2", "$y"), ("NAME", "@N"),])
        p = figure(
            # title="PCA based on: " + parameter,
            x_axis_label="PC 1 " + str(a) + "%",
            y_axis_label="PC 2 " + str(b) + "%",
            x_range=(-2, 6),
            y_range=(-4, 4.1),
            tools=[hover],
            plot_width=1000,
            plot_height=800,
        )
        p.add_tools(
            LassoSelectTool(), ZoomInTool(), ZoomOutTool(), SaveTool(), PanTool()
        )
        TCM_plot = p.circle(
            x="x", y="y", source=source5, color="mediumvioletred", size=5
        )
        AfroDB_plot = p.circle(x="x", y="y", source=source3, color="darkorange", size=5)
        NuBBEDB_plot = p.circle(
            x="x", y="y", source=source4, color="dodgerblue", size=5
        )
        FDA_plot = p.circle(x="x", y="y", source=source1, color="blueviolet", size=5)
        BFQ_plot = p.circle(x="x", y="y", source=source2, color="green", size=5)

        legend = Legend(
            items=[
                ("FDA", [FDA_plot]),
                ("BFQ", [BFQ_plot]),
                ("AfroDB", [AfroDB_plot]),
                ("NuBBEDB", [NuBBEDB_plot]),
                ("TCM", [TCM_plot]),
            ],
            location="center",
            orientation="vertical",
            click_policy="hide",
        )
        p.add_layout(legend, place="right")
        p.xaxis.axis_label_text_font_size = "20pt"
        p.yaxis.axis_label_text_font_size = "20pt"
        p.xaxis.axis_label_text_color = "black"
        p.yaxis.axis_label_text_color = "black"
        p.xaxis.major_label_text_font_size = "18pt"
        p.yaxis.major_label_text_font_size = "18pt"
        p.title.text_font_size = "22pt"
        #
        show(p)
        # save
        p.output_backend = "svg"
        export_svgs(
            p,
            filename="/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/"
            + "ChemSpace_PCA"
            + ".svg",
        )

    def plot_pca_fda_bio(self, parameter, a, b):
        result = self.result
        print(a, b)
        source1 = column_source(result, "FDA")
        source2 = column_source(result, "BIOFACQUIM")
        hover = HoverTool(tooltips=[("PCA 1", "$x"), ("PCA 2", "$y"), ("NAME", "@N"),])
        p = figure(
            # title="PCA based on: " + parameter,
            x_axis_label="PC 1 " + str(a) + "%",
            y_axis_label="PC 2 " + str(b) + "%",
            x_range=(-2, 6),
            y_range=(-4, 4.1),
            tools=[hover],
            plot_width=1000,
            plot_height=800,
        )
        p.add_tools(
            LassoSelectTool(), ZoomInTool(), ZoomOutTool(), SaveTool(), PanTool()
        )
        FDA_plot = p.circle(x="x", y="y", source=source1, color="blueviolet", size=5)
        BFQ_plot = p.circle(x="x", y="y", source=source2, color="green", size=5)

        legend = Legend(
            items=[("FDA", [FDA_plot]), ("BFQ", [BFQ_plot]),],
            location="center",
            orientation="vertical",
            click_policy="hide",
        )
        p.add_layout(legend, place="right")
        p.xaxis.axis_label_text_font_size = "20pt"
        p.yaxis.axis_label_text_font_size = "20pt"
        p.xaxis.axis_label_text_color = "black"
        p.yaxis.axis_label_text_color = "black"
        p.xaxis.major_label_text_font_size = "18pt"
        p.yaxis.major_label_text_font_size = "18pt"
        p.title.text_font_size = "22pt"
        #
        show(p)
        # save
        p.output_backend = "svg"
        export_svgs(
            p,
            filename="/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/"
            + "ChemSpace_PCA_FDA_Bio"
            + ".svg",
        )

