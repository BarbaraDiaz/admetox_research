"""
template to perform distribution plot with bokeh
"""

import pandas as pd
import numpy as np

import os
from scipy.stats import uniform
import scipy.special

from bokeh.io import show, output_file, export_png
from bokeh.models import (
    Title,
    LinearAxis,
    ColumnDataSource,
    LassoSelectTool,
    ZoomInTool,
    ZoomOutTool,
    SaveTool,
    HoverTool,
    PanTool,
    Legend,
)
from bokeh.plotting import figure
from bokeh.core.enums import LegendLocation
from bokeh.io import export, export_png, export_svgs


def comp_hist(result, Library, descriptor):
    """
    input:
        result (DataFrame)
        Library (str)
        descriptor (str, desired descriptor)
    output:
        hist
        edges
        X
        pdf
    """
    DF = result[result["Library"] == Library]
    print(Library)
    print(DF[descriptor].describe())
    data = np.array(DF[descriptor])
    X = np.sort(data)
    hist, edges = np.histogram(X, density=True)
    mu = np.mean(X)
    sigma = np.std(X)
    pdf = 1 / (sigma * np.sqrt(2 * np.pi)) * np.exp(-((X - mu) ** 2) / (2 * sigma ** 2))
    print("mu: ", mu, "sigma: ", sigma)
    return hist, edges, X, pdf


class PlotDistributions:
    def __init__(self, route, file_name, x_range, y_range):
        self.Data = pd.read_csv(f"{route}{file_name}")
        self.Data.head()
        self.x_range = x_range
        self.y_range = y_range
        print(self.Data.Library.unique())
        # print(self.Data.columns)

    def plot(self, parameter, descriptor):
        result = self.Data
        hist1, edges1, X1, pdf1 = comp_hist(result, "FDA", descriptor)
        hist2, edges2, X2, pdf2 = comp_hist(result, "BIOFACQUIM", descriptor)
        hist3, edges3, X3, pdf3 = comp_hist(result, "AfroDB", descriptor)
        hist4, edges4, X4, pdf4 = comp_hist(result, "NuBBEDB", descriptor)
        hist5, edges5, X5, pdf5 = comp_hist(result, "TCM", descriptor)
        hover = HoverTool(tooltips=[("X", "$x"), ("Y 2", "$y"), ("NAME", "@N"),])
        p = figure(
            # title=parameter,
            x_axis_label=parameter,
            y_axis_label="Pr(x)",
            tools=[hover],
            plot_width=1600,
            plot_height=800,
            x_range=self.x_range,
            y_range=self.y_range,
        )
        p.add_tools(
            LassoSelectTool(), ZoomInTool(), ZoomOutTool(), SaveTool(), PanTool()
        )
        p.axis.major_label_text_font_size = "24pt"
        p.xaxis.axis_label_text_font_size = "26pt"
        p.xaxis.axis_label_text_font = "arial"
        p.yaxis.axis_label_text_font_size = "26pt"
        p.yaxis.axis_label_text_font = "arial"
        #### HISTOGRAM ####
        # FDA_plot = p.quad(top=hist1, bottom=0, left=edges1[:-1], right=edges1[1:], fill_color="crimson", line_color="black", alpha=0.5)
        #####
        # BFQ_plot = p.quad(top=hist2, bottom=0, left=edges2[:-1], right=edges2[1:], fill_color="green", line_color="black", alpha=0.5)
        ####
        # AfroDB_plot = p.quad(top=hist3, bottom=0, left=edges3[:-1], right=edges3[1:], fill_color="darkorange", line_color="black", alpha=0.5)
        ####
        # NuBBEDB_plot = p.quad(top=hist4, bottom=0, left=edges4[:-1], right=edges4[1:], fill_color="dodgerblue", line_color="black", alpha=0.5)
        #######
        # QUIPRONAB_plot = p.quad(top=hist5, bottom=0, left=edges5[:-1], right=edges5[1:], fill_color="mediumvioletred", line_color="black", alpha=0.5)
        #### NORMAL DISTRIBUTION ####
        TCM_pdf = p.line(X5, pdf5, line_color="mediumvioletred", line_width=4, alpha=1)
        FDA_pdf = p.line(X1, pdf1, line_color="blueviolet", line_width=4, alpha=1)
        BFQ_pdf = p.line(X2, pdf2, line_color="green", line_width=4, alpha=1)
        AfroDB_pdf = p.line(X3, pdf3, line_color="darkorange", line_width=4, alpha=1)
        NuBBEDB_pdf = p.line(X4, pdf4, line_color="dodgerblue", line_width=4, alpha=1)
        legend = Legend(
            items=[
                ("FDA", [FDA_pdf]),
                ("BIOFACQUIM", [BFQ_pdf]),
                ("AfroDB", [AfroDB_pdf]),
                ("NuBBEDB", [NuBBEDB_pdf]),
                ("TCM", [TCM_pdf]),
            ],
            label_text_font_size="32pt",
            location="center",
            orientation="vertical",
            click_policy="hide",
        )
        p.add_layout(legend, place="right")
        show(p)
        p.output_backend = "svg"
        export_svgs(
            p,
            filename="/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/"
            + str(descriptor)
            + ".svg",
        )
