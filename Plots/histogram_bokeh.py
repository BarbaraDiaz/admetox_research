"""
Plot a single histogram with bokeh
"""
"""
support file to perform histogram plotting
"""
import pandas as pd
import numpy as np

import os
from scipy.stats import uniform
import scipy.special

from IPython.display import Image, SVG
from bokeh.layouts import gridplot, layout
from bokeh.io import show, output_file, export, export_png, export_svgs
from bokeh.models import (
    Title,
    LinearAxis,
    ColumnDataSource,
    LassoSelectTool,
    ZoomInTool,
    ZoomOutTool,
    SaveTool,
    HoverTool,
    PanTool,
    Legend,
)
from bokeh.plotting import figure
from bokeh.core.enums import LegendLocation


def comp_hist(result, Library, descriptor):
    """
    input:
        result (DataFrame)
        Library (str)
        descriptor (str, desired descriptor)
    output:
        hist
        edges
        X
        pdf
    """
    DF = result[result["Library"] == Library]
    print(Library)
    print(DF[descriptor].describe())
    data = np.array(DF[descriptor])
    X = np.sort(data)
    hist, edges = np.histogram(X, density=True)
    mu = np.mean(X)
    sigma = np.std(X)
    pdf = 1 / (sigma * np.sqrt(2 * np.pi)) * np.exp(-((X - mu) ** 2) / (2 * sigma ** 2))
    print("mu: ", mu, "sigma: ", sigma)
    return hist, edges, X, pdf


class Histogram:
    def __init__(self, route, file_name, x_range, y_range):

        self.Data = pd.read_csv(f"{route}{file_name}")
        self.x_range = x_range
        self.y_range = y_range

    def normal_dist(self, descriptor, letter):
        result = self.Data
        hist1, edges1, X1, pdf1 = comp_hist(result, "FDA", descriptor)
        hover = HoverTool(tooltips=[("X", "$x"), ("Y 2", "$y"), ("NAME", "@N"),])
        p = figure(  # title =  descriptor,
            x_axis_label=f'{letter}{"pdf"}',
            y_axis_label="Fraction",
            tools=[hover],
            plot_width=1200,
            plot_height=800,
            x_range=self.x_range,
            y_range=self.y_range,
        )
        p.add_tools(
            LassoSelectTool(), ZoomInTool(), ZoomOutTool(), SaveTool(), PanTool()
        )
        p.xaxis.major_label_text_font_size = "18pt"
        p.yaxis.major_label_text_font_size = "18pt"
        p.xaxis.axis_label_text_font_size = "25pt"
        p.xaxis.axis_label_text_font = "arial"
        p.yaxis.axis_label_text_font_size = "25pt"
        p.yaxis.axis_label_text_font = "arial"
        p.title.text_font_size = "25pt"
        #### NORMAL DISTRIBUTION ####
        FDA_pdf = p.line(X1, pdf1, line_color="blueviolet", line_width=4, alpha=1)
        legend = Legend(
            items=[("FDA", [FDA_pdf]),],
            label_text_font_size="30pt",
            location="center",
            orientation="horizontal",
            click_policy="hide",
        )
        p.add_layout(legend, place="below")
        return p

    def histogram(self, descriptor, letter, library, color):
        result = self.Data
        hist, edges, X, pdf = comp_hist(result, library, descriptor)
        hover = HoverTool(tooltips=[("X", "$x"), ("Y", "$y"), ("NAME", "@N"),])
        p = figure(  # title =  descriptor,
            x_axis_label=f"{letter}{library}",
            y_axis_label="Fraction",
            tools=[hover],
            plot_width=1200,
            plot_height=800,
            x_range=self.x_range,
            y_range=self.y_range,
        )
        p.add_tools(
            LassoSelectTool(), ZoomInTool(), ZoomOutTool(), SaveTool(), PanTool()
        )
        p.xaxis.major_label_text_font_size = "18pt"
        p.yaxis.major_label_text_font_size = "18pt"
        p.xaxis.axis_label_text_font_size = "25pt"
        p.xaxis.axis_label_text_font = "arial"
        p.yaxis.axis_label_text_font_size = "25pt"
        p.yaxis.axis_label_text_font = "arial"
        p.quad(
            top=hist,
            bottom=0,
            left=edges[:-1],
            right=edges[1:],
            fill_color=color,
            line_color="black",
        )
        return p

    def plot(self, descriptor):
        """
        output, grid visualization 
                svg files
        """
        plot = self.histogram(descriptor, "a)", "FDA", "blueviolet")
        show(plot)
