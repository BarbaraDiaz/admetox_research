"""
support file to perform histogram plotting with bokeh
"""
import pandas as pd
import numpy as np

import os
from scipy.stats import uniform
import scipy.special

from IPython.display import Image, SVG
from bokeh.layouts import gridplot, layout
from bokeh.io import show, output_file, export, export_png, export_svgs
from bokeh.models import (
    Title,
    LinearAxis,
    ColumnDataSource,
    LassoSelectTool,
    ZoomInTool,
    ZoomOutTool,
    SaveTool,
    HoverTool,
    PanTool,
    Legend,
)
from bokeh.plotting import figure
from bokeh.core.enums import LegendLocation


def comp_hist(result, Library, descriptor):
    """
    input:
        result (DataFrame)
        Library (str)
        descriptor (str, desired descriptor)
    output:
        hist
        edges
        X
        pdf
    """
    DF = result[result["Library"] == Library]
    print(Library)
    print(DF[descriptor].describe())
    data = np.array(DF[descriptor])
    X = np.sort(data)
    hist, edges = np.histogram(X, density=True)
    mu = np.mean(X)
    sigma = np.std(X)
    pdf = 1 / (sigma * np.sqrt(2 * np.pi)) * np.exp(-((X - mu) ** 2) / (2 * sigma ** 2))
    print("mu: ", mu, "sigma: ", sigma)
    return hist, edges, X, pdf


class GridHistogram:
    def __init__(self, route, file_name, x_range, y_range):

        self.Data = pd.read_csv(f"{route}{file_name}")
        self.x_range = x_range
        self.y_range = y_range

    def normal_dist(self, descriptor, letter):
        result = self.Data
        hist1, edges1, X1, pdf1 = comp_hist(result, "FDA", descriptor)
        hist2, edges2, X2, pdf2 = comp_hist(result, "BIOFACQUIM", descriptor)
        hist3, edges3, X3, pdf3 = comp_hist(result, "AfroDB", descriptor)
        hist4, edges4, X4, pdf4 = comp_hist(result, "NuBBEDB", descriptor)
        hist5, edges5, X5, pdf5 = comp_hist(result, "TCM", descriptor)
        hover = HoverTool(tooltips=[("X", "$x"), ("Y 2", "$y"), ("NAME", "@N"),])
        p = figure(  # title =  descriptor,
            x_axis_label=f'{letter}{"pdf"}',
            y_axis_label="Fraction",
            tools=[hover],
            plot_width=1200,
            plot_height=800,
            x_range=self.x_range,
            y_range=self.y_range,
        )
        p.add_tools(
            LassoSelectTool(), ZoomInTool(), ZoomOutTool(), SaveTool(), PanTool()
        )
        p.xaxis.major_label_text_font_size = "18pt"
        p.yaxis.major_label_text_font_size = "18pt"
        p.xaxis.axis_label_text_font_size = "25pt"
        p.xaxis.axis_label_text_font = "arial"
        p.yaxis.axis_label_text_font_size = "25pt"
        p.yaxis.axis_label_text_font = "arial"
        p.title.text_font_size = "25pt"
        #### NORMAL DISTRIBUTION ####
        FDA_pdf = p.line(X1, pdf1, line_color="blueviolet", line_width=4, alpha=1)
        BFQ_pdf = p.line(X2, pdf2, line_color="green", line_width=4, alpha=1)
        AfroDB_pdf = p.line(X3, pdf3, line_color="darkorange", line_width=4, alpha=1)
        NuBBEDB_pdf = p.line(X4, pdf4, line_color="dodgerblue", line_width=4, alpha=1)
        TCM_pdf = p.line(X5, pdf5, line_color="mediumvioletred", line_width=4, alpha=1)
        legend = Legend(
            items=[
                ("FDA", [FDA_pdf]),
                ("BIOFACQUIM", [BFQ_pdf]),
                ("AfroDB", [AfroDB_pdf]),
                ("NuBBEDB", [NuBBEDB_pdf]),
                ("TCM", [TCM_pdf]),
            ],
            label_text_font_size="30pt",
            location="center",
            orientation="horizontal",
            click_policy="hide",
        )
        p.add_layout(legend, place="below")
        return p

    def histogram(self, descriptor, letter, library, color):
        result = self.Data
        hist, edges, X, pdf = comp_hist(result, library, descriptor)
        hover = HoverTool(tooltips=[("X", "$x"), ("Y", "$y"), ("NAME", "@N"),])
        p = figure(  # title =  descriptor,
            x_axis_label=f"{letter}{library}",
            y_axis_label="Fraction",
            tools=[hover],
            plot_width=1200,
            plot_height=800,
            x_range=self.x_range,
            y_range=self.y_range,
        )
        p.add_tools(
            LassoSelectTool(), ZoomInTool(), ZoomOutTool(), SaveTool(), PanTool()
        )
        p.xaxis.major_label_text_font_size = "18pt"
        p.yaxis.major_label_text_font_size = "18pt"
        p.xaxis.axis_label_text_font_size = "25pt"
        p.xaxis.axis_label_text_font = "arial"
        p.yaxis.axis_label_text_font_size = "25pt"
        p.yaxis.axis_label_text_font = "arial"
        p.quad(
            top=hist,
            bottom=0,
            left=edges[:-1],
            right=edges[1:],
            fill_color=color,
            line_color="black",
        )
        return p

    def plot(self, descriptor):
        """
        output, grid visualization 
                svg files
        """
        FDA_plot = self.histogram(descriptor, "a)", "FDA", "blueviolet")
        # FDA_plot.title.text = descriptor
        # FDA_plot.title.text_font_size = "42pt"
        BFQ_plot = self.histogram(descriptor, "b)", "BIOFACQUIM", "green")
        AfroDB_plot = self.histogram(descriptor, "c)", "AfroDB", "darkorange")
        NuBBEDB_plot = self.histogram(descriptor, "d)", "NuBBEDB", "dodgerblue")
        TCM_plot = self.histogram(descriptor, "e)", "TCM", "mediumvioletred")
        normal_dist = self.normal_dist(descriptor, "f)")
        plots = [FDA_plot, BFQ_plot, AfroDB_plot, NuBBEDB_plot, TCM_plot, normal_dist]
        final = gridplot(
            [
                [FDA_plot, BFQ_plot],
                [AfroDB_plot, NuBBEDB_plot],
                [TCM_plot, normal_dist],
            ],
        )
        export_png(
            final,
            filename="/home/babs/Documents/DIFACQUIM/admetox_research/Plots/Gridplots/"
            + descriptor
            + "grid.png",
        )
        show(final)
        # save as svg
        for i in plots:
            i.output_backend = "svg"
            export_svgs(
                i,
                filename="/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/plot"
                + str(i)
                + ".svg",
            )
