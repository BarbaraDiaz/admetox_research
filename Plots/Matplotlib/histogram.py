"""
histograms examples with matplotlib
"""
import numpy as np
import matplotlib.pyplot as plt

np.random.seed(19680801)

n_bins = 10
x = np.random.randn(1000, 3)

fig, ((ax0, ax1)) = plt.subplots(nrows=1, ncols=2)

colors = ["red", "tan", "lime"]
ax0.hist(x, n_bins, density=True, histtype="bar", color=colors, label=colors)
ax0.legend(prop={"size": 10})
ax0.set_title("bars with legend")


# Make a multiple-histogram of data-sets with different length.
x_multi = [np.random.randn(n) for n in [10000, 5000, 2000]]
ax1.hist(x_multi, n_bins, histtype="bar")
ax1.set_title("different sample sizes")

fig.tight_layout()
plt.show()
# http://docs.astropy.org/en/stable/visualization/histogram.html

