from PIL import Image
from io import BytesIO
import numpy

"""
Read individual .tiff files
merge in a matplotlib grid
save manually
"""
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.gridspec as gridspec

ima = Image.open(
    "/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/LogP/a.tiff"
)
# ima.show()
###read a with matplotlib
# ima = mpimg.imread(
#    "/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/LogP/a.tiff"
# )
imb = Image.open(
    "/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/LogP/b.tiff"
)
imc = mpimg.imread(
    "/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/LogP/c.tiff"
)
imd = mpimg.imread(
    "/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/LogP/d.tiff"
)
ime = mpimg.imread(
    "/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/LogP/e.tiff"
)
imf = mpimg.imread(
    "/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/LogP/f.tiff"
)

##grid
# fig = plt.figure(figsize=[16, 9])
fig = plt.figure(figsize=[16, 9])
###a)###
ax0 = fig.add_subplot(121)
ax0.imshow(ima)
ax0.axis("off")
###b)###
ax1 = fig.add_subplot(122)
ax1.imshow(imb)
ax1.axis("off")
###c)###
#####
plt.tight_layout()
fig.suptitle("Consensus Log P", fontsize=16)
plt.show()

# save figure
# (1) save the image in memory in PNG format
png1 = BytesIO()
fig.savefig(png1, format="png")

# (2) load this image into PIL
png2 = Image.open(png1)

# (3) save as TIFF
png2.save("3dPlot.tiff")
png1.close()
