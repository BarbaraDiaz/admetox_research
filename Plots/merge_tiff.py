from PIL import Image
import numpy

"""
Read individual .tiff files
merge in a matplotlib grid
save manually
"""
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.gridspec as gridspec

ima = Image.open(
    "/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/LogP/a.tiff"
)
# ima.show()
###read a with matplotlib
# ima = mpimg.imread(
#    "/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/LogP/a.tiff"
# )
imb = Image.open(
    "/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/LogP/b.tiff"
)
imc = mpimg.imread(
    "/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/LogP/c.tiff"
)
imd = mpimg.imread(
    "/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/LogP/d.tiff"
)
ime = mpimg.imread(
    "/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/LogP/e.tiff"
)
imf = mpimg.imread(
    "/home/babs/Documents/DIFACQUIM/admetox_research/Plots/SVG_files/LogP/f.tiff"
)

##grid
# fig = plt.figure(figsize=[16, 9])
fig = plt.figure(figsize=[16, 9])
gs = gridspec.GridSpec(nrows=2, ncols=3)
###a)###
ax0 = fig.add_subplot(gs[0, 0])
ax0.imshow(ima)
ax0.axis("off")
###b)###
ax1 = fig.add_subplot(gs[0, 1])
ax1.imshow(imb)
ax1.axis("off")
###c)###
ax1 = fig.add_subplot(gs[0, 2])
ax1.imshow(imc)
ax1.axis("off")
###d)###
ax1 = fig.add_subplot(gs[1, 0])
ax1.imshow(imd)
ax1.axis("off")
###e###
ax1 = fig.add_subplot(gs[1, 1])
ax1.imshow(ime)
ax1.axis("off")
###f###
ax1 = fig.add_subplot(gs[1, 2])
ax1.imshow(imf)
ax1.axis("off")
#####
plt.tight_layout()
fig.suptitle("Consensus Log P", fontsize=16)
plt.show()
