def selfCombine(list2Combine, length):
    listCombined = (
        str(["list2Combine[i" + str(i) + "]" for i in range(length)]).replace("'", "")
        + "for i0 in range(len( list2Combine ) )"
    )
    if length > 1:
        listCombined += (
            str(
                [
                    " for i"
                    + str(i)
                    + " in range( i"
                    + str(i - 1)
                    + ", len( list2Combine ) )"
                    for i in range(1, length)
                ]
            )
            .replace("', '", " ")
            .replace("['", "")
            .replace("']", "")
        )

    listCombined = "[" + listCombined + "]"
    listCombined = eval(listCombined)

    return listCombined


list2Combine = [
    "CYP1A2 inhibitor",
    "CYP2C19 inhibitor",
    "CYP2C9 inhibitor",
    "CYP2D6 inhibitor",
    "CYP3A4 inhibitor",
]
