"""
identify compounds that at least inhibite 2 CYPs
    input: Master_results.csv
    return : compounds_multiple_cyp_inhibition.csv
"""
from collections import Counter
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns


class ClassifyCompounds:
    def __init__(self, route, file_name):
        self.Data = pd.read_csv(f"{route}{file_name}")
        features = [
            "Library",
            "Name",
            "CYP1A2 inhibitor",
            "CYP2C19 inhibitor",
            "CYP2C9 inhibitor",
            "CYP2D6 inhibitor",
            "CYP3A4 inhibitor",
        ]
        self.Data = self.Data[features]
        print(self.Data.Library.unique())

    def library_search(self, library):
        data = self.Data
        names = self.Data.Name.tolist()
        DF = data[data["Library"] == library].reset_index()
        print(DF.head())
        print(DF.shape,)
        cyps = [
            "CYP1A2 inhibitor",
            "CYP2C19 inhibitor",
            "CYP2C9 inhibitor",
            "CYP2D6 inhibitor",
            "CYP3A4 inhibitor",
        ]
        data = DF[cyps].to_numpy()
        print(data)
        plt.subplots(figsize=(15, 10))
        ax = sns.heatmap(
            data=data,
            xticklabels=cyps,
            # linewidths=0.02,
            # vmin = 0,
            # vmax = 1,
            # annot = True,
            cmap="coolwarm",
            cbar=False,
        )
        plt.yticks([])
        # plt.xlabel(fontsize=24)
        plt.ylabel(str(library), fontsize=24)
        plt.show()
        # save
        plt.savefig("heatmap_inhibition_" + str(library) + ".png")
        return DF

    def search(self):
        FDA = self.library_search("FDA")
        BIOFACQUIM = self.library_search("BIOFACQUIM")
        AFRO = self.library_search("AfroDB")
        NUBBEDB = self.library_search("NuBBEDB")
        TCM = self.library_search("TCM")

    def percentage(self, library, elements, cyps):
        data = self.Data
        DF = data[data["Library"] == library]
        percentage = list()
        for i in cyps:
            c = Counter(list(DF[i]))  # c, count
            n = c[1]  # "YES" frecuency
            percentage_n = round((n / elements) * 100)  # rename n
            percentage.append(percentage_n)
            continue
        print(percentage)
        return percentage

    def get_percentages(self):
        cyps = [
            "CYP1A2 inhibitor",
            "CYP2C19 inhibitor",
            "CYP2C9 inhibitor",
            "CYP2D6 inhibitor",
            "CYP3A4 inhibitor",
        ]
        data = {
            "CYP inhibition": cyps,
            "FDA": self.percentage("FDA", 1692, cyps),
            "BIOFACQUIM": self.percentage("BIOFACQUIM", 531, cyps),
            "AfroDB": self.percentage("AfroDB", 954, cyps),
            "NuBBEDB": self.percentage("NuBBEDB", 1333, cyps),
            "TCM": self.percentage("TCM", 2000, cyps),
        }
        data = pd.DataFrame.from_dict(data)
        data.to_csv("percentage_cyp_inhibition_per_library.csv")
        print(data)


# Leer datos
a = ClassifyCompounds(
    "/home/babs/Documents/DIFACQUIM/admetox_research/metabolism/",
    "compounds_multiple_cyp_inhibition.csv",
)
a.search()
# a.get_percentages()

