"""
identify compounds that at least inhibite 2 CYPs
    input: Master_results.csv
    return : compounds_multiple_cyp_inhibition.csv
"""
import pandas as pd
import numpy as np

class IdentifyCompounds:

    def __init__(self, route, file_name):
        Data = pd.read_csv(f'{route}{file_name}')
        print(" Initial compounds: ", Data.shape[0])
        self.Data = Data.drop(["Unnamed: 0",   "Unnamed: 0.1",  "Unnamed: 0.1.1", "Unnamed: 0.1.2"], axis = 1)
        
    def find_cyp_inhibition(self):
        data = self.Data
        CYP1A2 = data[data["CYP1A2 inhibitior"] == "Yes"]
        CYP2C19 = data[data["CYP2C19 inhibitior"] == "Yes"]
        CYP2C9 = data[data["CYP2C9 inhibitior"] == "Yes"]
        CYP2D6 = data[data["CYP2D6 inhibitior"] == "Yes"]
        CYP3A4 = data[data["CYP3A4 inhibitior"] == "Yes"]
        #print(
        #    CYP1A2.shape,
        #    CYP2C19.shape,
        #    CYP2C9.shape,
        #    CYP2D6.shape,
        #    CYP3A4.shape)
        frames = [CYP1A2, CYP2C19, CYP2C9, CYP2D6, CYP3A4]
        multiple = pd.concat(frames, axis = 0)
        return multiple

    def at_least_two(self):
        multiple = self.find_cyp_inhibition()
        a = multiple[(multiple['CYP1A2 inhibitor']=="Yes") & (multiple['CYP2C19 inhibitor']=="Yes")]
        b = multiple[(multiple['CYP1A2 inhibitor']=="Yes") & (multiple['CYP2C9 inhibitor']=="Yes")]
        c = multiple[(multiple['CYP1A2 inhibitor']=="Yes") & (multiple['CYP2D6 inhibitor']=="Yes")]
        d = multiple[(multiple['CYP1A2 inhibitor']=="Yes") & (multiple['CYP3A4 inhibitor']=="Yes")]
        e = multiple[(multiple['CYP2C19 inhibitor']=="Yes") & (multiple['CYP2C9 inhibitor']=="Yes")]
        f = multiple[(multiple['CYP2C19 inhibitor']=="Yes") & (multiple['CYP2D6 inhibitor']=="Yes")]
        g = multiple[(multiple['CYP2C19 inhibitor']=="Yes") & (multiple['CYP3A4 inhibitor']=="Yes")] 
        h = multiple[(multiple['CYP2C9 inhibitor']=="Yes") & (multiple['CYP2D6 inhibitor']=="Yes")]
        i = multiple[(multiple['CYP2C9 inhibitor']=="Yes") & (multiple['CYP3A4 inhibitor']=="Yes")]
        j = multiple[(multiple['CYP2D6 inhibitor']=="Yes") & (multiple['CYP3A4 inhibitor']=="Yes")]
        frames = [a,b,c,d,e,f,g,h,i,j]
        double_inhibition = pd.concat(frames, axis = 0) 
        return double_inhibition
       
    def delete_duplicates(self):
        """
        delete duplicates by name
        """
        double_inhibition = self.at_least_two()
        result = double_inhibition.drop_duplicates(subset="Name", keep = "first")
        result.to_csv("compounds_multiple_cyp_inhibition.csv")
        print("compounds that inhibite at least 2 CYPs:", result.shape[0])

#Leer datos
a = IdentifyCompounds(
                "/home/barbara/Documents/DIFACQUIM/admetox_research/Libraries/", 
                "Master_results.csv"
)
a.delete_duplicates()

