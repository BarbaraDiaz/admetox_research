"""
identify compounds percentage of 2,3 and 4 inhibition
    input: compounds_multiple_cyp_inhibition.csv
    process: count inhibition frequency for 2,3 and 4
            and convert to percentage
    return : percentage_cyp_inhibition_2_3_4.csv (csv)
             percentage_cyp_inhibition_2_3_4.png (heatmap)
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from compibations import selfCombine

# get combinations of 3 elements
list2Combine = [
    "CYP1A2 inhibitor",
    "CYP2C19 inhibitor",
    "CYP2C9 inhibitor",
    "CYP2D6 inhibitor",
    "CYP3A4 inhibitor",
]
listCombined = selfCombine(list2Combine, 3)


class IdentifyCompounds:
    def __init__(self, route, file_name):
        self.Data = pd.read_csv(f"{route}{file_name}")
        print(" Initial compounds: ", self.Data.shape[0])
        features = [
            "Name",
            "Library",
            "CYP1A2 inhibitor",
            "CYP2C19 inhibitor",
            "CYP2C9 inhibitor",
            "CYP2D6 inhibitor",
            "CYP3A4 inhibitor",
        ]
        self.Data = self.Data[features]
        # print(d)

    def counter(self):
        """
        add a counter row
        """
        data = self.Data
        df_count = data["count"] = data.apply(
            lambda row: row["CYP1A2 inhibitor"]
            + row["CYP2C19 inhibitor"]
            + row["CYP2C9 inhibitor"]
            + row["CYP2D6 inhibitor"]
            + row["CYP3A4 inhibitor"],
            axis=1,
        )
        final = pd.concat([data, df_count], axis=1)
        final = final.drop([0], axis=1)
        # print(final.head())
        # final.to_csv("count_frequency_cyps_inhibition.csv")
        return final

    def percentages(self, library, size):
        DF = self.counter()
        DF = DF[DF["Library"] == library]
        print(DF.shape[0])
        dos = DF[DF["count"] == 2]
        tres = DF[DF["count"] == 3]
        cuatro = DF[DF["count"] == 4]
        percentage = [
            dos.shape[0] * 100 / size,
            tres.shape[0] * 100 / size,
            cuatro.shape[0] * 100 / size,
        ]
        return percentage


# Leer datos
a = IdentifyCompounds(
    "/home/babs/Documents/DIFACQUIM/admetox_research/metabolism/",
    "compounds_multiple_cyp_inhibition.csv",
)
FDA = a.percentages("FDA", 1692)
BIOFACQUIM = a.percentages("BIOFACQUIM", 531)
AFRO = a.percentages("AfroDB", 954)
NUBBE = a.percentages("NuBBEDB", 1333)
TCM = a.percentages("TCM", 2000)
data = np.array([FDA, BIOFACQUIM, AFRO, NUBBE, TCM])
data = np.around(data, decimals=2)
plt.subplots(figsize=(15, 10))
ax = sns.heatmap(
    data=data,
    xticklabels=["2", "3", "4"],
    yticklabels=["FDA", "BIOFACQUIM", "AfroDB", "NuBBE", "TCM"],
    linewidths=0.2,
    # vmin = 0,
    # vmax = 1,
    annot=True,
    cmap="coolwarm",
    cbar=True,
)
plt.ylabel("Libraries", fontsize=18)
plt.xlabel("Predicted multiple cyp inhibition", fontsize=18)
plt.show()
# save
plt.savefig("percentage_cyp_inhibition_2_3_4.png")
data = pd.DataFrame(
    data=data,
    columns=["2", "3", "4"],
    index=["FDA", "BIOFACQUIM", "AfroDB", "NuBBE", "TCM"],
)
data.to_csv("percentage_cyp_inhibition_2_3_4.csv")
print(data)
