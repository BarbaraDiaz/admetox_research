import os
from os import listdir
from os.path import isfile, join

"""Obtain location manually with pwd command"""

def existing_files(location):
    mypath  = str(location)
    print(mypath)
    files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    print(files)
    return files