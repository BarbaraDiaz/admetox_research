"""
Merge Data frames
1)Pegar los individualmente
2)Pegar los ids
"""

#importar librerias
import pandas as pd
import numpy as  np
import os

from Read_csv_files import *

class MergeDataFrames:
    
    """PARAMETERS
    funcion, seleccionar el número de DF a pegar
    referencia, indique un nombre para el archivo de salida
    *archivos, el nombre de los csvs que desea pegar"""
    def __init__(self, input_file, Library):
        import os
        Data = pd.read_csv(f'/home/barbara/Documents/DIFACQUIM/admetox_research/Input_files/{input_file}', sep= ",")
        print("Data shape: ", Data.shape)
        features = ['ID', 'ID','SMILES']
        Data = Data[features]
        Data.columns = ['Name', 'ID_Database','Input Smiles']
        Data["Library"] = [Library for i in range(len(Data["Input Smiles"]))]
        print(Data.head())
        self.Data = Data
        print("Input file shape: ", self.Data.head())
        
    def merge(self, function, output_file, sep, route, *archivos ):
        frames  = function(sep, route, *archivos)  
        DF = pd.concat(frames, axis = 0).reset_index()
        DF = DF.drop("index", axis = 1)
        print("DF shape: ", DF.shape)
        return DF
        
    def merge_ID(self, function, output_file, sep, route, *archivos ): 
        Data = self.Data.reset_index()
        DF = self.merge(function, output_file, sep, route, *archivos)
        final_DF = pd.concat([Data, DF], axis=1)
        final_DF = final_DF.drop("index", axis = 1)
        #write out file
        final_DF.to_csv(output_file, sep= ",")
        print("File is ready")
        return final_DF

#MERGE SUBSETS
final_DF = MergeDataFrames("TCM_sample.csv", "TCM").merge_ID(twenty, #function
                                                        "/home/barbara/Documents/DIFACQUIM/admetox_research/Libraries/Libraries_swissADME/TCM_results_swiss.csv", #output_file
                                                        "," , #sep
                                                        "/home/barbara/Documents/DIFACQUIM/admetox_research/ADMET_swiss/TCM/", #ruta
                                                        "swiss_TCM_0.csv",   
                                                        "swiss_TCM_1.csv",  
                                                        "swiss_TCM_2.csv",  
                                                        "swiss_TCM_3.csv",  
                                                        "swiss_TCM_4.csv",  
                                                        "swiss_TCM_5.csv",  
                                                        "swiss_TCM_6.csv",  
                                                        "swiss_TCM_7.csv",  
                                                        "swiss_TCM_8.csv",  
                                                        "swiss_TCM_9.csv",
                                                        "swiss_TCM_10.csv",  
                                                        "swiss_TCM_11.csv",  
                                                        "swiss_TCM_12.csv",  
                                                        "swiss_TCM_13.csv",  
                                                        "swiss_TCM_14.csv",  
                                                        "swiss_TCM_15.csv",   
                                                        "swiss_TCM_16.csv",  
                                                        "swiss_TCM_17.csv",  
                                                        "swiss_TCM_18.csv",  
                                                        "swiss_TCM_19.csv",
                                                        )


                                            
                                                        