"""
Merge final results
"""

#importar librerias
import pandas as pd
import numpy as  np
import os

from Read_csv_files import *

def merge(function, output_file, sep, route, *archivos ):
    frames  = function(sep, route, *archivos)  
    DF = pd.concat(frames, axis = 0)
    #DF = DF.drop("index", axis = 1)
    #DF = DF.drop("level_0", axis =1)
    print(DF.head())
    print("DF shape: ", DF.shape)
    DF.to_csv(output_file, sep = sep)
    return DF

#MERGE SUBSETS 
DF = merge(five, #function
        "/home/barbara/Documents/DIFACQUIM/admetox_research/Libraries/Final_pkcsm.csv", #output_file
        "," , #sep
        "/home/barbara/Documents/DIFACQUIM/admetox_research/Libraries/Libraries_pkc/", #ruta
        "FDA_results_pkcsm.csv", 
        "Biofacquim_results_pkcsm.csv",
        "AfroDB_results_pkcsm.csv",
        "NuBBE_results_pkcsm.csv",
        "TCM_results_pkcsm.csv"
        )
                                            
                                                        