import pandas as pd 

"""
read csv files, and return DataFrames
"""
def two(sep, route, *files):
    df1 = pd.read_csv(f'{route}{files[0]}', sep = sep)
    df2 = pd.read_csv(f'{route}{files[1]}', sep = sep)
    return df1, df2

def four(sep, route, *files):
    df1 = pd.read_csv(f'{route}{files[0]}', sep = sep)
    df2 = pd.read_csv(f'{route}{files[1]}', sep = sep)
    df3 = pd.read_csv(f'{route}{files[2]}', sep = sep)
    df4 = pd.read_csv(f'{route}{files[3]}', sep = sep)
    return df1, df2, df3, df4

def five(sep, route, *files):
    df1 = pd.read_csv(f'{route}{files[0]}', sep = sep)
    df2 = pd.read_csv(f'{route}{files[1]}', sep = sep)
    df3 = pd.read_csv(f'{route}{files[2]}', sep = sep)
    df4 = pd.read_csv(f'{route}{files[3]}', sep = sep)
    df5 = pd.read_csv(f'{route}{files[4]}', sep = sep)
    return df1, df2, df3, df4, df5

def six(sep, route, *files):
    df1 = pd.read_csv(f'{route}{files[0]}', sep = sep)
    df2 = pd.read_csv(f'{route}{files[1]}', sep = sep)
    df3 = pd.read_csv(f'{route}{files[2]}', sep = sep)
    df4 = pd.read_csv(f'{route}{files[3]}', sep = sep)
    df5 = pd.read_csv(f'{route}{files[4]}', sep = sep)
    df6 = pd.read_csv(f'{route}{files[5]}', sep = sep)
    return df1, df2, df3, df4, df5, df6 

def ten(sep, route, *files):
    df1 = pd.read_csv(f'{route}{files[0]}', sep = sep)
    df2 = pd.read_csv(f'{route}{files[1]}', sep = sep)
    df3 = pd.read_csv(f'{route}{files[2]}', sep = sep)
    df4 = pd.read_csv(f'{route}{files[3]}', sep = sep)
    df5 = pd.read_csv(f'{route}{files[4]}', sep = sep)
    df6 = pd.read_csv(f'{route}{files[5]}', sep = sep)
    df7 = pd.read_csv(f'{route}{files[6]}', sep = sep)
    df8 = pd.read_csv(f'{route}{files[7]}', sep = sep)
    df9 = pd.read_csv(f'{route}{files[8]}', sep = sep)
    df10 = pd.read_csv(f'{route}{files[9]}', sep = sep)
    return df1, df2, df3, df4, df5, df6, df7, df8, df9, df10 




def eight(sep, route, *files):
    df1 = pd.read_csv(f'{route}{files[0]}', sep = sep)
    df2 = pd.read_csv(f'{route}{files[1]}', sep = sep)
    df3 = pd.read_csv(f'{route}{files[2]}', sep = sep)
    df4 = pd.read_csv(f'{route}{files[3]}', sep = sep)
    df5 = pd.read_csv(f'{route}{files[4]}', sep = sep)
    df6 = pd.read_csv(f'{route}{files[5]}', sep = sep)
    df7 = pd.read_csv(f'{route}{files[6]}', sep = sep)
    df8 = pd.read_csv(f'{route}{files[7]}', sep = sep)
    return df1, df2, df3, df4, df5, df6, df7, df8

def thirteen(sep, route, *files):
    df1 = pd.read_csv(f'{route}{files[0]}', sep = sep)
    df2 = pd.read_csv(f'{route}{files[1]}', sep = sep)
    df3 = pd.read_csv(f'{route}{files[2]}', sep = sep)
    df4 = pd.read_csv(f'{route}{files[3]}', sep = sep)
    df5 = pd.read_csv(f'{route}{files[4]}', sep = sep)
    df6 = pd.read_csv(f'{route}{files[5]}', sep = sep)
    df7 = pd.read_csv(f'{route}{files[6]}', sep = sep)
    df8 = pd.read_csv(f'{route}{files[7]}', sep = sep)
    df9 = pd.read_csv(f'{route}{files[8]}', sep = sep)
    df10 = pd.read_csv(f'{route}{files[9]}', sep = sep)
    df11 = pd.read_csv(f'{route}{files[10]}', sep = sep)
    df12 = pd.read_csv(f'{route}{files[11]}', sep = sep)
    df13 = pd.read_csv(f'{route}{files[12]}', sep = sep)
    return df1, df2, df3, df4, df5, df6, df7, df8, df9, df10, df11, df12, df13

def fourteen(sep, route, *files):
    df1 = pd.read_csv(f'{route}{files[0]}', sep = sep)
    df2 = pd.read_csv(f'{route}{files[1]}', sep = sep)
    df3 = pd.read_csv(f'{route}{files[2]}', sep = sep)
    df4 = pd.read_csv(f'{route}{files[3]}', sep = sep)
    df5 = pd.read_csv(f'{route}{files[4]}', sep = sep)
    df6 = pd.read_csv(f'{route}{files[5]}', sep = sep)
    df7 = pd.read_csv(f'{route}{files[6]}', sep = sep)
    df8 = pd.read_csv(f'{route}{files[7]}', sep = sep)
    df9 = pd.read_csv(f'{route}{files[8]}', sep = sep)
    df10 = pd.read_csv(f'{route}{files[9]}', sep = sep)
    df11 = pd.read_csv(f'{route}{files[10]}', sep = sep)
    df12 = pd.read_csv(f'{route}{files[11]}', sep = sep)
    df13 = pd.read_csv(f'{route}{files[12]}', sep = sep)
    df14 = pd.read_csv(f'{route}{files[13]}', sep = sep)
    return df1, df2, df3, df4, df5, df6, df7, df8, df9, df10, df11, df12, df13, df14

def thirteen(sep, route, *files):
    df1 = pd.read_csv(f'{route}{files[0]}', sep = sep)
    df2 = pd.read_csv(f'{route}{files[1]}', sep = sep)
    df3 = pd.read_csv(f'{route}{files[2]}', sep = sep)
    df4 = pd.read_csv(f'{route}{files[3]}', sep = sep)
    df5 = pd.read_csv(f'{route}{files[4]}', sep = sep)
    df6 = pd.read_csv(f'{route}{files[5]}', sep = sep)
    df7 = pd.read_csv(f'{route}{files[6]}', sep = sep)
    df8 = pd.read_csv(f'{route}{files[7]}', sep = sep)
    df9 = pd.read_csv(f'{route}{files[8]}', sep = sep)
    df10 = pd.read_csv(f'{route}{files[9]}', sep = sep)
    df11 = pd.read_csv(f'{route}{files[10]}', sep = sep)
    df12 = pd.read_csv(f'{route}{files[11]}', sep = sep)
    df13 = pd.read_csv(f'{route}{files[12]}', sep = sep)
    return df1, df2, df3, df4, df5, df6, df7, df8, df9, df10, df11, df12, df13

def fourteen(sep, route, *files):
    df1 = pd.read_csv(f'{route}{files[0]}', sep = sep)
    df2 = pd.read_csv(f'{route}{files[1]}', sep = sep)
    df3 = pd.read_csv(f'{route}{files[2]}', sep = sep)
    df4 = pd.read_csv(f'{route}{files[3]}', sep = sep)
    df5 = pd.read_csv(f'{route}{files[4]}', sep = sep)
    df6 = pd.read_csv(f'{route}{files[5]}', sep = sep)
    df7 = pd.read_csv(f'{route}{files[6]}', sep = sep)
    df8 = pd.read_csv(f'{route}{files[7]}', sep = sep)
    df9 = pd.read_csv(f'{route}{files[8]}', sep = sep)
    df10 = pd.read_csv(f'{route}{files[9]}', sep = sep)
    df11 = pd.read_csv(f'{route}{files[10]}', sep = sep)
    df12 = pd.read_csv(f'{route}{files[11]}', sep = sep)
    df13 = pd.read_csv(f'{route}{files[12]}', sep = sep)
    df14 = pd.read_csv(f'{route}{files[13]}', sep = sep)
    return df1, df2, df3, df4, df5, df6, df7, df8, df9, df10, df11, df12, df13, df14

def sixteen(sep, route, *files):
    df1 = pd.read_csv(f'{route}{files[0]}', sep = sep)
    df2 = pd.read_csv(f'{route}{files[1]}', sep = sep)
    df3 = pd.read_csv(f'{route}{files[2]}', sep = sep)
    df4 = pd.read_csv(f'{route}{files[3]}', sep = sep)
    df5 = pd.read_csv(f'{route}{files[4]}', sep = sep)
    df6 = pd.read_csv(f'{route}{files[5]}', sep = sep)
    df7 = pd.read_csv(f'{route}{files[6]}', sep = sep)
    df8 = pd.read_csv(f'{route}{files[7]}', sep = sep)
    df9 = pd.read_csv(f'{route}{files[8]}', sep = sep)
    df10 = pd.read_csv(f'{route}{files[9]}', sep = sep)
    df11 = pd.read_csv(f'{route}{files[10]}', sep = sep)
    df12 = pd.read_csv(f'{route}{files[11]}', sep = sep)
    df13 = pd.read_csv(f'{route}{files[12]}', sep = sep)
    df14 = pd.read_csv(f'{route}{files[13]}', sep = sep)
    df15 = pd.read_csv(f'{route}{files[14]}', sep = sep)
    df16 = pd.read_csv(f'{route}{files[15]}', sep = sep)
    return df1, df2, df3, df4, df5, df6, df7, df8, df9, df10, df11, df12, df13, df14, df15, df16


def twenty(sep, route, *files):
    df1 = pd.read_csv(f'{route}{files[0]}', sep = sep)
    df2 = pd.read_csv(f'{route}{files[1]}', sep = sep)
    df3 = pd.read_csv(f'{route}{files[2]}', sep = sep)
    df4 = pd.read_csv(f'{route}{files[3]}', sep = sep)
    df5 = pd.read_csv(f'{route}{files[4]}', sep = sep)
    df6 = pd.read_csv(f'{route}{files[5]}', sep = sep)
    df7 = pd.read_csv(f'{route}{files[6]}', sep = sep)
    df8 = pd.read_csv(f'{route}{files[7]}', sep = sep)
    df9 = pd.read_csv(f'{route}{files[8]}', sep = sep)
    df10 = pd.read_csv(f'{route}{files[9]}', sep = sep)
    df11 = pd.read_csv(f'{route}{files[10]}', sep = sep)
    df12 = pd.read_csv(f'{route}{files[11]}', sep = sep)
    df13 = pd.read_csv(f'{route}{files[12]}', sep = sep)
    df14 = pd.read_csv(f'{route}{files[13]}', sep = sep)
    df15 = pd.read_csv(f'{route}{files[14]}', sep = sep)
    df16 = pd.read_csv(f'{route}{files[15]}', sep = sep)
    df17 = pd.read_csv(f'{route}{files[16]}', sep = sep)
    df18 = pd.read_csv(f'{route}{files[17]}', sep = sep)
    df19 = pd.read_csv(f'{route}{files[18]}', sep = sep)
    df20 = pd.read_csv(f'{route}{files[19]}', sep = sep)
    return df1, df2, df3, df4, df5, df6, df7, df8, df9, df10, df11, df12, df13, df14, df15, df16, df17, df18, df19, df20

def twentynine(sep, route, *files):
    df1 = pd.read_csv(f'{route}{files[0]}', sep = sep)
    df2 = pd.read_csv(f'{route}{files[1]}', sep = sep)
    df3 = pd.read_csv(f'{route}{files[2]}', sep = sep)
    df4 = pd.read_csv(f'{route}{files[3]}', sep = sep)
    df5 = pd.read_csv(f'{route}{files[4]}', sep = sep)
    df6 = pd.read_csv(f'{route}{files[5]}', sep = sep)
    df7 = pd.read_csv(f'{route}{files[6]}', sep = sep)
    df8 = pd.read_csv(f'{route}{files[7]}', sep = sep)
    df9 = pd.read_csv(f'{route}{files[8]}', sep = sep)
    df10 = pd.read_csv(f'{route}{files[9]}', sep = sep)
    df11 = pd.read_csv(f'{route}{files[10]}', sep = sep)
    df12 = pd.read_csv(f'{route}{files[11]}', sep = sep)
    df13 = pd.read_csv(f'{route}{files[12]}', sep = sep)
    df14 = pd.read_csv(f'{route}{files[13]}', sep = sep)
    df15 = pd.read_csv(f'{route}{files[14]}', sep = sep)
    df16 = pd.read_csv(f'{route}{files[15]}', sep = sep)
    df17 = pd.read_csv(f'{route}{files[16]}', sep = sep)
    df18 = pd.read_csv(f'{route}{files[17]}', sep = sep)
    df19 = pd.read_csv(f'{route}{files[18]}', sep = sep)
    df20 = pd.read_csv(f'{route}{files[19]}', sep = sep)
    df21 = pd.read_csv(f'{route}{files[20]}', sep = sep)
    df22 = pd.read_csv(f'{route}{files[21]}', sep = sep)
    df23 = pd.read_csv(f'{route}{files[22]}', sep = sep)
    df24 = pd.read_csv(f'{route}{files[23]}', sep = sep)
    df25 = pd.read_csv(f'{route}{files[24]}', sep = sep)
    df26 = pd.read_csv(f'{route}{files[25]}', sep = sep)
    df27 = pd.read_csv(f'{route}{files[26]}', sep = sep)
    df28 = pd.read_csv(f'{route}{files[27]}', sep = sep)
    df29 = pd.read_csv(f'{route}{files[28]}', sep = sep)
    l = [df1, df2, df3, df4, df5, df6, df7, df8, df9, df10, df11, df12, df13, df14, df15, df16, df17, df18, df19, df20, df21, df22, df23, df24, df25, df26, df27, df28, df29]
    for i in l:
        print(i.shape)
    return df1, df2, df3, df4, df5, df6, df7, df8, df9, df10, df11, df12, df13, df14, df15, df16, df17, df18, df19, df20, df21, df22, df23, df24, df25, df26, df27, df28, df29 