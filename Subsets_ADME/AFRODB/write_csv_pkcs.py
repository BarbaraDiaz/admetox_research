import pandas as pd 
import numpy as np

def write(csv, output):
    input_data = pd.read_csv(csv)
    output_data = np.asarray(input_data["Smiles"])
    output_data = np.transpose(output_data)
    output_data = pd.DataFrame(output_data, columns = ["SMILES"],  )
    output_data.to_csv(output)
    print(output_data.head())

write("Afro_subset_0.csv", "s_Afro_subset_0.csv")
write("Afro_subset_1.csv", "s_Afro_subset_1.csv")
write("Afro_subset_2.csv", "s_Afro_subset_2.csv")
write("Afro_subset_3.csv", "s_Afro_subset_3.csv")
write("Afro_subset_4.csv", "s_Afro_subset_4.csv")
write("Afro_subset_5.csv", "s_Afro_subset_5.csv")
write("Afro_subset_6.csv", "s_Afro_subset_6.csv")
write("Afro_subset_7.csv", "s_Afro_subset_7.csv")
write("Afro_subset_8.csv", "s_Afro_subset_8.csv")
write("Afro_subset_9.csv", "s_Afro_subset_9.csv")