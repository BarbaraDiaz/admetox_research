import pandas as pd 
import numpy as np

def write(csv, output):
    input_data = pd.read_csv(csv)
    output_data = np.asarray(input_data["SMILES"])
    output_data = np.transpose(output_data)
    output_data = pd.DataFrame(output_data, columns = ["SMILES"],  )
    output_data.to_csv(output)
    print(output_data.head())

write("FDA_subset_16.csv", "s_FDA_subset_16.csv")