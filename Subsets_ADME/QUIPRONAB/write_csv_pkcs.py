import pandas as pd 
import numpy as np

def write(csv, output):
    input_data = pd.read_csv(csv)
    output_data = np.asarray(input_data["SMILES"])
    output_data = np.transpose(output_data)
    output_data = pd.DataFrame(output_data, columns = ["SMILES"],  )
    output_data.to_csv(output)
    print(output_data.head())

write("QUIPRONAB_subset_0.csv", "s_QUIPRONAB_subset_0.csv")
write("QUIPRONAB_subset_1.csv", "s_QUIPRONAB_subset_1.csv")
write("QUIPRONAB_subset_2.csv", "s_QUIPRONAB_subset_2.csv")
write("QUIPRONAB_subset_3.csv", "s_QUIPRONAB_subset_3.csv")
write("QUIPRONAB_subset_4.csv", "s_QUIPRONAB_subset_4.csv")
write("QUIPRONAB_subset_5.csv", "s_QUIPRONAB_subset_5.csv")