import pandas as pd 
import numpy as np

def write(csv, output):
    input_data = pd.read_csv(csv)
    output_data = np.asarray(input_data["Canonical SMILES"])
    output_data = np.transpose(output_data)
    output_data = pd.DataFrame(output_data, columns = ["SMILES"],  )
    output_data.to_csv(output)
    print(output_data.head())

write("swiss_Biofacquim_0.csv", "s_Biofacquim_subset_0.csv")
write("swiss_Biofacquim_1.csv", "s_Biofacquim_subset_1.csv")
write("swiss_Biofacquim_2.csv", "s_Biofacquim_subset_2.csv")
write("swiss_Biofacquim_3.csv", "s_Biofacquim_subset_3.csv")
