"""
compute statics for a single descriptor
"""
import pandas as pd
import numpy as np


def stat_descriptor(route, file_name, descriptor):
    """
    input:
        route: route file
        file_name: master csv (contain results from both servers)
        descriptor: target descriptor
    ourput:
        stats as .csv file
    """
    data = pd.read_csv(f"{route}{file_name}")
    # print(data.columns)
    d = data.groupby("Library")[descriptor].describe().round(3)
    d.to_csv(f"stats_{descriptor}.csv")
    print(d)


stat_descriptor(
    # "/home/barbara/Documents/DIFACQUIM/admetox_research/Libraries/",
    "/home/babs/Documents/DIFACQUIM/admetox_research/Libraries/",
    "Master_results.csv",
    "Silicos-IT LogSw",
)

